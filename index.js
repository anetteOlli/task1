//Dom elements
const elSelectLaptops = document.getElementById('laptops');
const elPay = document.getElementById('pay');
const elBalance = document.getElementById('balance');
const elWorkButton = document.getElementById('work');
const elBankButton = document.getElementById('bank');
const elLoanButton = document.getElementById('applyForLoan');
const elPurchaseButton = document.getElementById('purchaseLaptop');
const elLoan = document.getElementById('loan');
const elLaptopImg = document.getElementById('laptopImage');
const elLaptopName = document.getElementById('laptopName');
const elPrice = document.getElementById('price');
const elLaptopDescription = document.getElementById('description');
const elLaptopFeatures = document.getElementById('laptopFeatures');
const elRepayLoanButton = document.getElementById('repayLoan');



let balance = 0;
let pay = 0;
let loan = 0;
let selectedLaptop;
let havePurchasedComputer = false;

function Laptop (name, price, img, description, features) {
    this.name = name;
    this.price = price;
    this.img = img;
    this.description = description;
    this.features = features;
}

const laptops = [ 
    new Laptop(
        "Pink hello kitty laptop", 
        1000, 
        "laptopPics/kittyLaptop.jpg" , 
        "Fabulous economic laptop", 
        ["fluffy screen", "pink keyboard"]),
    new Laptop(
        "Commodore 64 vintage computer",
        2000,
        "laptopPics/commodore64.jpg",
        "Excellent vintage laptop",
        ["fire hazard", "sandwich grill", "monitor"]
    ), 
    new Laptop(
        "Dos vintage computer",
        3000,
        "laptopPics/dosLaptop.jpg",
        "This runs all your favorite DOS-games",
        ["turns on", "runs DOOM" , "no GUI"]
    ),
    new Laptop(
        "Mac vintage computer",
        4000,
        "laptopPics/mac.jpg",
        "Will probably start",
        ["impress your friends", "the genuine mac experience"]
    )
];

//At the entry point of the app. The repay loan button and buy now button is not needed.
elRepayLoanButton.style.visibility = "hidden";
elPurchaseButton.style.visibility = "hidden";

//Generates options based on the laptops array.
laptops.forEach(element => {
    let option = document.createElement('option');
    option.text = element.name;
    option.className = 'laptop';
    elSelectLaptops.add(option);
});

//Adds money to pay account
elWorkButton.addEventListener("click", event => {
    pay += 100;
    render(balance, pay, loan);
});

//Handles bank button.
elBankButton.addEventListener("click", event => {
    balance += pay;
    pay = 0;
    render(balance, pay, loan);
});


//Handles loan functionality. 
elLoanButton.addEventListener("click", event => {
    //ellibleLoanAmount is the max loan the customer can receive.
    let ellibleLoanAmount = (loan > 0  && !havePurchasedComputer) ? 0 : (balance - loan) * 2;
    if (ellibleLoanAmount > 0){
        let loanedPrompt = prompt(`Please specify the amount you want to loan. The amount you can apply for: ${ellibleLoanAmount} Kr`);
        if(loanedPrompt != null && loanedPrompt !== ""){
            let loanApplication = parseInt(loanedPrompt);
            if (loanApplication <= ellibleLoanAmount) {
                loan = loanApplication;
                balance += loanApplication;
                havePurchasedComputer = false;
                render(balance, pay, loan);
            } else {
                alert("you can't loan that much!");
            }
        }
    } else {
        alert("you are not ellible for a loan!");
    }

});





//Detects user has selected a different laptop:
elSelectLaptops.addEventListener("change", event => {
    const selectedValue = elSelectLaptops.options[elSelectLaptops.selectedIndex].text;
    selectedLaptop = laptops.find(element => element.name === selectedValue);
    if (selectedLaptop) {
        elLaptopImg.src = selectedLaptop.img;
        elLaptopName.innerText = selectedLaptop.name;
        elPrice.innerText = `Price: ${selectedLaptop.price} Kr`;
        elLaptopDescription.innerText = selectedLaptop.description;
        elPurchaseButton.style.visibility = "visible";
        renderLaptopFeatures(selectedLaptop.features);
    } else {
        elLaptopImg.src = '';
        elLaptopName.innerText = '';
        elPrice.innerText = '';
        elLaptopDescription.innerText = '';
        elPurchaseButton.style.visibility = "hidden";
        renderLaptopFeatures();
    }

})


//Codeblock for purchasing selected laptop.
elPurchaseButton.addEventListener("click", event => {
    if (selectedLaptop) {
        if(selectedLaptop.price > balance && pay === 0) {
            alert("not enough money!");
        } else if (pay > 0) {
            alert("you must transfer pay money to balance with bank button!");
        } else {
            balance -= selectedLaptop.price;
            havePurchasedComputer = true;
            alert(`Congratulations! You now own ${selectedLaptop.name}`);
            render(balance, pay, loan);
        }

    } else {
        alert("no laptop selected");
    }
});


//Handles loan repayment. Pays down loans from bank balance.
elRepayLoanButton.addEventListener("click", event =>{
    if (loan > balance) {
        alert('Not enough money to repay loan');
    } else {
        balance -= loan;
        loan = 0;
        render(balance, pay, loan);
    }
});

const render = (balance, pay, loan) =>{
    elBalance.innerText = balance;
    elPay.innerText = pay;
    elLoan.innerText = loan;

    loan > 0 ? elRepayLoanButton.style.visibility = "visible" : elRepayLoanButton.style.visibility = "hidden";
    
}

const renderLaptopFeatures = features => {
    elLaptopFeatures.innerHTML = '';
    if (!features){
        return;
    }
    features.forEach(feature => {
        const elFeatureItem = document.createElement('li');
        elFeatureItem.innerText = feature;
        elLaptopFeatures.appendChild(elFeatureItem);
    });
}
